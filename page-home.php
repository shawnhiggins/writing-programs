<?php
/*
 Template Name: Home Page
*/
?>
 <?php  
        // This check the url and make sure the elements switch..
        $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        $switchURL = 'wp';
        //echo $switchURL;
        if (strpos($url,'uwc') !== false) {
            //echo '<h3>UWC exists.</h3>';
            $switchURL = 'uwc';
        } elseif (strpos($url,'esl') !== false) {
            //echo '<h3>ESL exists.</h3>';
            $switchURL = 'esl';
        } else {
           // echo '<h3> No ESL.</h3>';
            $switchURL = 'wp';
        }
        //echo $switchURL;
    ?>

<?php get_header(); 
    if ( is_front_page() ) { 
    // FrontPage Display...
?>
			<div id="main-content" role="main">
				
				
				<?php // If set to Slider
					if(get_field('hero_type', 'option') == "single") { ?>
					<div id="slider">
						<ul id="bxslider">
							<?php if( have_rows('menu_items') ): ?>
							<?php while( have_rows('menu_items') ): the_row(); ?>
							<?php
								$slider_title = get_sub_field('title');
								$slider_description = get_sub_field('title_acronym');
								$slider_link = get_sub_field('link');
								$silder_image = get_sub_field('image');                                                                      
								$slider_icon = get_sub_field('icon');
								if( !empty($silder_image) ): 
									// vars
									$url = $silder_image['url'];
									$title = $silder_image['title'];
									// thumbnail
									$size = 'home-hero';
									$slide = $silder_image['sizes'][ $size ];
									$width = $silder_image['sizes'][ $size . '-width' ];
									$height = $silder_image['sizes'][ $size . '-height' ];
								endif;
                                                                      
                                if( !empty($slider_icon) ): 
									// vars
									$url = $slider_icon['url'];
									$title = $slider_icon['title'];
									// thumbnail
									$size = 'large-icons';
									$icon = $slider_icon['sizes'][ $size ];
									$width = $slider_icon['sizes'][ $size . '-width' ];
									$height = $slider_icon['sizes'][ $size . '-height' ];
								endif;
							?>		
							<?php if( $slider_link ): ?>
							<a href="<?php echo $slider_link; ?>" class="hero-link">
							<?php endif; ?>
							<li class="<?php echo $slider_description; ?>"> 
                                <div class="b-img" style="background-image: url('<?php echo $slide; ?>');">
                                    <?php if( $icon ): ?>
										<div class="icon"><img src="<?php echo $icon; ?>" /></div>
										<?php endif; ?>
								<div class="content">
									<div class="slider-content">
										<?php if( $slider_title ): ?>
										<h2><?php echo $slider_title; ?></h2>
										<?php endif; ?>
										<?php if( $slider_descriptions ): ?>
										<p><?php echo $slider_description; ?></p>
										<?php endif; ?>
									</div>
								</div>
                                </div>
                                <div class="front-menu">                                
                                    <nav role="navigation" aria-labelledby="main navigation" class="desktop">
                                        <?php wp_nav_menu(array(
                                            'container' => false,
                                            'menu' => __( $slider_description.' Main Menu', 'bonestheme' ),
                                            'menu_class' => $slider_description.'-main-nav',
                                            'theme_location' => $slider_description.'-main-nav',
                                            'before' => '',
                                            'after' => '',
                                            'depth' => 1,
                                        )); ?>
                                    </nav>
                                </div>
							</li>
							<?php if( $slider_link ): ?>
							</a>
							<?php endif; ?>
							<?php endwhile; ?>
							<?php endif; ?>
						</ul>
					</div>
				<?php } ?>
                
					<?php if(get_field('enable_donation', 'option') == "enable") { ?>
					<div class="container give-back">
                        <div class="content">
                            <?php if(get_field('link_type', 'option') == "internal") { ?>
                            <a href="<?php the_field('donation_page', 'option'); ?>" class="btn give">
                            <?php }?>
                            <?php if(get_field('link_type', 'option') == "external") { ?>
                            <a href="<?php the_field('donation_link', 'option'); ?>" class="btn give" target="_blank">
                            <?php }?>
                               <img src="<?php echo get_template_directory_uri(); ?>/library/images/givingheart.png" alt="<?php the_field('button_text', 'option'); ?>" class="heart-logo" />
                            <?php the_field('button_text', 'option'); ?></a>
                            <?php if(get_field('supporting_text', 'option')) { ?>
                            <span><?php the_field('supporting_text', 'option'); ?></span>
                            <?php }?>
                        </div>
					</div>
					<?php }?>
			</div>
<?php 
    } else {
    // This is not the FrontPage display...
?>
			<div id="main-content" role="main">
				<?php // If set to Single/Random 
					if(get_field('hero_type', 'option') == "single") {
					$rows = get_field('hero_image' ); // get all the rows
					$rand_row = $rows[ array_rand( $rows ) ]; // get a random row
					$silder_image = $rand_row['image' ]; // get the sub field value 
					$slider_description = $rand_row['description' ]; // get the sub field value 
					$slider_link = $rand_row['link' ]; // get the sub field value 			
					if( !empty($silder_image) ): 
						// vars
						$url = $silder_image['url'];
						$title = $silder_image['title'];
						// thumbnail
						$size = 'home-hero';
						$slide = $silder_image['sizes'][ $size ];
						$width = $silder_image['sizes'][ $size . '-width' ];
						$height = $silder_image['sizes'][ $size . '-height' ];
					endif;
				?>
				<?php if( $silder_image ): ?>
				<?php if( $slider_link ): ?>
				<a href="<?php echo $home_button_link; ?>" class="hero-link">
				<?php endif; ?>
					<div id="hero" class="desktop" data-image-src="<?php echo $slide; ?>" data-parallax="scroll" data-speed=".8" data-natural-width="1400">
						<div class="content">
						<?php if( $slider_description ): ?>
							<div class="hero-description">
								<?php echo $slider_description; ?>
							</div>
						<?php endif; ?>
						</div>
					</div>
					<div id="hero" class="mobile-hero" style="background-image:url('<?php echo $slide; ?>');">
					</div>
				<?php if( $slider_link ): ?>
				</a>
				<?php endif; ?>
				<?php endif; ?>
				<?php } ?>
				
				<?php // If set to Slider
					if(get_field('hero_type', 'option') == "slider") { ?>
					<script type="text/javascript">
						jQuery("document").ready(function($) {
							$(document).ready(function(){
							  $('#bxslider').bxSlider({
							  	autoHover: true,
							  	auto: false,
							  });
							});
						});
					</script>
					<div id="slider">
						<ul id="bxslider">
							<?php if( have_rows('hero_image') ): ?>
							<?php while( have_rows('hero_image') ): the_row(); ?>
							<?php
								$slider_title = get_sub_field('title');
								$slider_description = get_sub_field('description');
								$slider_link = get_sub_field('link');
								$silder_image = get_sub_field('image');
								if( !empty($silder_image) ): 
									// vars
									$url = $silder_image['url'];
									$title = $silder_image['title'];
									// thumbnail
									$size = 'home-hero';
									$slide = $silder_image['sizes'][ $size ];
									$width = $silder_image['sizes'][ $size . '-width' ];
									$height = $silder_image['sizes'][ $size . '-height' ];
								endif;
							?>		
							<?php if( $slider_link ): ?>
							<a href="<?php echo $slider_link; ?>" class="hero-link">
							<?php endif; ?>
							<li style="background-image: url('<?php echo $slide; ?>');">
								<div class="content">
									<div class="slider-content">
										<?php if( $slider_title ): ?>
										<h2><?php echo $slider_title; ?></h2>
										<?php endif; ?>
										<?php if( $slider_description ): ?>
										<p><?php echo $slider_description; ?></p>
										<?php endif; ?>
									</div>
								</div>
							</li>
							<?php if( $slider_link ): ?>
							</a>
							<?php endif; ?>
							<?php endwhile; ?>
							<?php endif; ?>
						</ul>
					</div>
				<?php } ?>
				<div class="content">
					<div class="col news-col">
                        <?php if($switchURL == 'esl'){ ?>                        
						
					<div class="col about-col two">
						<?php $about_query = new WP_Query( 'pagename=esl/about' );
						while ( $about_query->have_posts() ) : $about_query->the_post();
						$do_not_duplicate = $post->ID; ?>
						<h3><?php the_title(); ?></h3>
						<p><?php
						$content = get_the_content();
						$trimmed_content = wp_trim_words( $content, 100, '...' );
						echo $trimmed_content;
						?></p>
						<a class="btn" href="<?php the_permalink() ?>">Learn More <span class="hidden">About Us</span></a>
						<?php endwhile; ?>
					</div>
                        
                        <?php }else{ ?>
                        
						
                            <?php if($switchURL == 'wp'){ ?>
                                <h3>Latest News</h3>
						          <?php query_posts( array ( 'showposts' => 4	) ); ?>
                            <?php }else{ ?>
                                <h3>In The News</h3>                        
						          <?php query_posts( array ( 'category_name' => 'uwc'	) ); ?>
                            <?php } ?>                       
                        
						<ul>
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
							<a href="<?php the_permalink() ?>">
								<li>
									<?php if ( has_post_thumbnail() ) {
										$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'article-thumb' );
										$url = $thumb['0']; ?>
										<img src="<?=$url?>" alt="<?php the_title(); ?>" />
									<?php } ?>
									<div class="news-item">
										<h4><?php the_title(); ?></h4>
										<p><?php
											$content = get_the_content();
											$trimmed_content = wp_trim_words( $content, 27, '...' );
											echo $trimmed_content;
										?></p>
									</div>
								</li>
							</a>							
							<?php endwhile; ?>
						</ul>
						<?php endif; ?>
						<a class="btn" href="<?php echo home_url(); ?>/category/<?php echo $switchURL; ?>/">View All<span class="hidden"> News</span></a>
                        
                        <?php } ?>                        
					</div>
					<div class="col quicklinks-col">
						<h3>Quick Links</h3>
						<?php wp_nav_menu(array(
							'container' => '',
							'menu' => __( $switchURL.'Quick Links', 'bonestheme' ),
							'menu_class' => $switchURL.'-quick-links',
							'theme_location' => $switchURL.'-quick-links',
							'before' => '',
							'after' => '',
							'link_before' => '<h4>',
							'link_after' => '</h4>',
							'depth' => 0,
							'walker' => new Description_Walker
						)); ?>
					</div>
                    <?php if($switchURL == 'uwc'){ ?>
					<div class="col events-col">
						<h3>Make an Appointment</h3>
                        <?php if(get_field('appointment_url')){ ?>
                            <?php the_field('appointment_url'); ?>
                        <?php } ?>
                        <?php if(get_field('appointment_title')){ ?>
                            <?php the_field('appointment_title'); ?>
                        <?php } ?>
                        <?php if(get_field('appointment_description')){ ?>
                            <?php the_field('appointment_description'); ?>
                        <?php } ?>
					</div>
                    
                    <?php } else { ?>
					<div class="col events-col">
						<ul>
							<?php if ( is_active_sidebar( 'events-sidebar' ) ) : ?>
								<?php dynamic_sidebar( 'events-sidebar' ); ?>				
							<?php else : endif; ?>
						</ul>
					</div>
                    <?php } ?>
				</div>
			</div>
<?php } ?>
<?php get_footer(); ?>