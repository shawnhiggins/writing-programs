				<?php // For posts
				if (is_single() || is_category() || is_search() || is_page()) { ?>
				<div class="col side feed" role="complementary">
					<?php if ( is_active_sidebar( 'news-sidebar' ) ) :  ?>
					<?php dynamic_sidebar( 'news-sidebar' ); ?>
					<?php else : endif; ?>
					<?php if ( is_active_sidebar( 'events-sidebar' ) ) :  ?>
					<?php dynamic_sidebar( 'events-sidebar' ); ?>
					<?php else : endif; ?>
				</div>
				<?php } ?>
				<?php // For pages
				if (is_page() || is_404()) { ?>
				<div class="col side">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php
								// If a Students subpage
								if (is_tree(1343) || get_field('menu_select') == "students") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Students', 'bonestheme' ),
										'menu_class' => 'students-nav',
										'theme_location' => 'students-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Students</h3> <ul>%3$s</ul>'
									));
								}
								// If a Outreach subpage 
								if (is_tree(1323) || is_tree(1338) || get_field('menu_select') == "outreach") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Outreach', 'bonestheme' ),
										'menu_class' => 'outreach-nav',
										'theme_location' => 'outreach-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Outreach</h3> <ul>%3$s</ul>'
									));
								}
								// If a Writing Resources subpage
								if (is_tree(939) || get_field('menu_select') == "writing") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Writing', 'bonestheme' ),
										'menu_class' => 'writing-nav',
										'theme_location' => 'writing-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Writing Resources</h3> <ul>%3$s</ul>'
									));
								}
								// If a UCLA Writes! subpage
								if (is_tree(1350) || get_field('menu_select') == "uclawrites") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Ucla Writes!', 'bonestheme' ),
										'menu_class' => 'uclawrites-nav',
										'theme_location' => 'uclawrites-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>UCLA Writes!</h3> <ul>%3$s</ul>'
									));
								}                        
								// If a About subpage
								if (is_tree(8630) || get_field('menu_select') == "about") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'About', 'bonestheme' ),
										'menu_class' => 'about-nav',
										'theme_location' => 'about-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>About</h3> <ul>%3$s</ul>'
									));
								}
								// If a Graduate subpage
								if (is_tree(1863) || get_field('menu_select') == "graduate") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Graduate', 'bonestheme' ),
										'menu_class' => 'graduate-nav',
										'theme_location' => 'graduate-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Graduate</h3> <ul>%3$s</ul>'
									));
								}
								// If an Undergraduate subpage
								if (is_tree(860) || get_field('menu_select') == "undergraduate") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Undergraduate', 'bonestheme' ),
									   	'menu_class' => 'undergrad-nav',
									   	'theme_location' => 'undergrad-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Undergraduate</h3> <ul>%3$s</ul>'
									));
								}
								// If an UWC About subpage
								if (is_tree(1694) || get_field('menu_select') == "aboutuwc") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'About UWC', 'bonestheme' ),
									   	'menu_class' => 'aboutuwc-nav',
									   	'theme_location' => 'aboutuwc-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>About UWC</h3> <ul>%3$s</ul>'
									));
								}
								// If a UWC For Students subpage
								if (is_tree(1704) || get_field('menu_select') == "forstudents") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'For Students', 'bonestheme' ),
										'menu_class' => 'forstudents-nav',
										'theme_location' => 'forstudents-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>For Students</h3> <ul>%3$s</ul>'
									));
								}
								// If an UWC For Faculty subpage
								if (is_tree(1710) || get_field('menu_select') == "forfaculty") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'For Faculty', 'bonestheme' ),
									   	'menu_class' => 'forfaculty-nav',
									   	'theme_location' => 'forfaulty-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>For Faculty</h3> <ul>%3$s</ul>'
									));
								}
								// For Search, 404's, or other pages you want to use it on
								// Replace 9999 with id of parent page
								if (is_tree(9999) || is_search() || is_404() || is_page('contact') || is_page('about') || get_field('menu_select') == "general") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Main Menu', 'bonestheme' ),
										'menu_class' => 'side-nav',
										'theme_location' => 'main-nav',
										'before' => '',
										'after' => '',
										'depth' => 1,
										'items_wrap' => '<h3>Main Menu</h3> <ul>%3$s</ul>'
									));
								}
							?>
						</nav>
						<?php if ( is_page( 'give' )  ) : ?>
							<?php dynamic_sidebar( 'give-sidebar' ); ?>
						<?php else : ?>
						<?php endif; ?>
					</div>
				</div>
				<?php } ?>