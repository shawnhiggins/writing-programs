<!doctype html>
<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js ie lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js ie lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js ie lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js ie"><!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php wp_title(''); ?></title>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-touch-icon.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#cc3333">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
		<?php wp_head(); ?>
		<?php // drop Google Analytics Here ?>
		<?php // end analytics ?>
	</head>
    <?php  
        // This check the url and make sure the elements switch..
        $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        $switchURL = 'wp';
        //echo $switchURL;
        if (strpos($url,'uwc') !== false) {
            //echo '<h3>UWC exists.</h3>';
            $switchURL = 'uwc';
        } elseif (strpos($url,'esl') !== false) {
            //echo '<h3>ESL exists.</h3>';
            $switchURL = 'esl';
        } else {
           // echo '<h3> No ESL.</h3>';
            $switchURL = 'wp';
        }
       // echo $switchURL;
    ?>

	<body <?php body_class(); ?>>
		<a href="#main-content" class="hidden skip">Skip to main content</a>
		<div id="container">
			<header role="banner" class="top">
				<div class="content">
					<a href="<?php echo home_url(); ?>" rel="nofollow">
						<img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-logo.png" alt="UCLA" class="university-logo" /><span class="hidden">UCLA</span>
						<?php // H1 if homepage, H2 otherwise.
							if ( is_front_page() ) { ?>
						<h1><img src="<?php echo get_template_directory_uri(); ?>/library/images/<?php echo $switchURL; ?>-dept-logo.png" alt="<?php the_field('department_name', 'option'); ?>" class="dept-logo" /><span class="hidden"><?php the_field('department_name', 'option'); ?></span></h1>
						<?php } else { ?>
						<h2><img src="<?php echo get_template_directory_uri(); ?>/library/images/<?php echo $switchURL; ?>-dept-logo.png" alt="<?php the_field('department_name', 'option'); ?>" class="dept-logo" /><span class="hidden"><?php the_field('department_name', 'option'); ?></span></h2>
						<?php }?>
					</a>
					<?php if(get_field('enable_donation', 'option') == "enable") { ?>
					<div class="give-back">
						<?php if(get_field('link_type', 'option') == "internal") { ?>
						<a href="<?php the_field('donation_page', 'option'); ?>" class="btn give">
						<?php }?>
						<?php if(get_field('link_type', 'option') == "external") { ?>
						<a href="<?php the_field('donation_link', 'option'); ?>" class="btn give" target="_blank">
						<?php }?>
						<?php the_field('button_text', 'option'); ?></a>
						<?php if(get_field('supporting_text', 'option')) { ?>
						<span><?php the_field('supporting_text', 'option'); ?></span>
						<?php }?>
					</div>
					<?php }?>
				</div>
				<nav role="navigation" aria-labelledby="main navigation" class="desktop">
					<?php wp_nav_menu(array(
						'container' => false,
						'menu' => __( $switchURL.' Main Menu', 'bonestheme' ),
						'menu_class' => $switchURL.'-main-nav',
						'theme_location' => $switchURL.'-main-nav',
						'before' => '',
						'after' => '',
						'depth' => 2,
					)); ?>
				</nav>
				<nav role="navigation" aria-labelledby="main navigation" class="mobile">
					<?php wp_nav_menu(array(
						'container' => false,
						'menu' => __( $switchURL.' Main Menu', 'bonestheme' ),
						'theme_location' => $switchURL.'-main-nav',
						'before' => '',
						'after' => '',
						'depth' => 2,
						'items_wrap' => '<ul id="mobile-nav">%3$s</ul>'
					)); ?>
				</nav>
				<?php get_search_form(); ?>
			</header>
            <!--// added spotlight by shawn -->
            <?php get_template_part( 'section-menu' ); ?>
                
			<?php 
				// Don't do any of the below if homepage
				if ( is_front_page() ) { }
				// Breadcrumb everywhere else
				elseif ( is_single() || is_category( $category ) || is_archive() ) { 
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
					}
				}
				else {
					// Only show hero image on a page or post
					if ( has_post_thumbnail() && is_single() || has_post_thumbnail() && is_page() ) {
						$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'hero' );
						$url = $thumb['0']; ?>
			<div id="hero" data-image-src="<?=$url?>" data-parallax="scroll" data-speed=".8">
				&nbsp;
			</div>
			<?php }
					// And show breadcrumb
					if ( function_exists('yoast_breadcrumb') ) {
                        if(!is_page_template( 'page-home.php' ) ) {
						yoast_breadcrumb('<div class="breadcrumbs"><div class="content">','</div></div>');
                        }
					} 
				} 
			?>