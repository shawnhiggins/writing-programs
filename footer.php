 <?php  
        // This check the url and make sure the elements switch..
        $url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        $switchURL = 'wp';
        //echo $switchURL;
        if (strpos($url,'uwc') !== false) {
            //echo '<h3>UWC exists.</h3>';
            $switchURL = 'uwc';
        } elseif (strpos($url,'esl') !== false) {
            //echo '<h3>ESL exists.</h3>';
            $switchURL = 'esl';
        } else {
           // echo '<h3> No ESL.</h3>';
            $switchURL = 'wp';
        }
       // echo $switchURL;
    ?>			<footer role="contentinfo">
				<div class="content">
					<nav role="navigation" aria-labelledby="footer navigation">
						<!--// ?php wp_nav_menu(array(
							'container' => '',
							'menu' => __( 'Footer Links', 'bonestheme' ),
							'menu_class' => 'footer-nav',
							'theme_location' => 'footer-links',
							'before' => '',
							'after' => '',
							'depth' => 1,
						)); ? //-->
                        
                        <?php wp_nav_menu(array(
                            'container' => false,
                            'menu' => __( $switchURL.' Main Menu', 'bonestheme' ),
                            'menu_class' => $switchURL.'-main-nav',
                            'theme_location' => $switchURL.'-main-nav',
                            'before' => '',
                            'after' => '',
                            'depth' => 1,
					   )); ?>
                        
						<ul class="social-links">
						<?php if(get_field('facebook', 'option')) { ?>
							<li class="icon"><a href="<?php the_field('facebook', 'option'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/library/images/fb-icon.png" alt="Facebook" class="facebook" /></a></li>
						<?php } if(get_field('twitter', 'option')) { ?>
							<li class="icon"><a href="<?php the_field('twitter', 'option'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/library/images/twitter-icon.png" alt="Twitter" class="twitter" /></a></li>
						<?php } if(get_field('contact_us', 'option')) { ?>
							<li class="icon"><a href="<?php the_field('contact_us', 'option'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/library/images/mail-icon.png" alt="Contact Us" class="email" /></a></li>
						<?php } ?>
						</ul>
					</nav>
					<?php get_search_form(); ?>
					<div class="copyright">
						<a href="http://ucla.edu" class="university-logo"><img src="<?php echo get_template_directory_uri(); ?>/library/images/ucla-logo-white.png" alt="UCLA" /></a>
						<p><?php the_field('department_name', 'option'); ?> is part of the <a href="http://humanities.ucla.edu">Humanities Division</a> within <a href="http://college.ucla.edu/">UCLA College</a>.<br />
						<?php the_field('main_office_location', 'option'); ?> <span>|</span> <?php the_field('city_state', 'option'); ?> <?php the_field('zip_code', 'option'); ?> 
						<?php if (get_field('phone_number', 'option')) { ?>	
						<span>|</span> <strong>P:</strong> <?php the_field('phone_number', 'option'); ?>
						<?php } 
						if (get_field('fax_number', 'option')) { ?>	
						<span>|</span> <strong>F:</strong> <?php the_field('fax_number', 'option'); ?>
						<?php } 
						if (get_field('dept_email_address', 'option')) { ?> 
						 <span>|</span> <strong>E:</strong> <a href="mailto:<?php the_field('dept_email_address', 'option'); ?>"><?php the_field('dept_email_address', 'option'); ?></a>
						<?php } ?>
						<br />
						University of California &copy; <?php echo date('Y'); ?> UC Regents</p>
					</div>
				</div>
			</footer>
		<?php wp_footer(); ?>
	</body>
</html>